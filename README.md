# Personal site:
Prod: http://shaunbur.ch  
Staging: https://shazbot89.gitlab.io/shaunbur.ch/  

# Credits:
**Identity by HTML5 UP**  
[html5up.net](html5up.net) | @ajlkn  
Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)  
Modified by @shazbot89  

**Demo Images:**  
Unsplash ([unsplash.com](https://unsplash.com))

**Icons:**  
Font Awesome ([fortawesome.github.com/Font-Awesome](fortawesome.github.com/Font-Awesome))

**Other:**  
html5shiv.js (@afarkas @jdalton @jon_neal @rem)  
CSS3 Pie ([css3pie.com](css3pie.com))  
Respond.js ([j.mp/respondjs](j.mp/respondjs))  
Skel ([skel.io](skel.io))
